// require('./css/bootstrap-theme.scss');
// require('bootstrap-slider/src/sass/bootstrap-slider.scss');
// require('./css/bootstrap-multiselect.scss');
require('./css/main.scss');
// require('./css/main-page.scss');
// require('./css/style.scss');

require('./js/main.js');

/** WEB PAGES AND IMAGES BELOW */

require('./index.html');

require('./full-map.html');

// import 'file-loader?name=[name].[ext]!./index.html';

require('./aboutcity.html');
require('./img/2gis.png');
require('./img/map.png');
require('./img/new1.png');
require('./img/photo.png');

require('./best.html');

require('./detail.html');

require('./unicum.html');

require('./unicumdetail.html');
