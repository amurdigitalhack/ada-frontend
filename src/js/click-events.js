$(() => {
    if($(".full-map-page").length){
        const DG = window.DG;

        const $markerList = $("#marker-list");

        const $map = $("#map");

        const map = $map.data("DG:map");
        const markers = $map.data("DG:markers");

        map.on("click", (e)=>{

            $markerList.append(`<li class="list-group-item">Ширина: ${e.latlng.lat}<br/>Долгота: ${e.latlng.lng}</li>`);

            DG.marker(e.latlng).addTo(markers);

        });

        $markerList.children().first().click(() => {

            $markerList.children().not(":first").remove();

            markers.clearLayers();
        });
    }
});
