$(() => {
    if($(".full-map-page").length){
        const DG = window.DG;

        markers = DG.featureGroup();

        let map = DG.map('map', {
            center: [54.98, 82.89],
            zoom: 13
        });

        markers.addTo(map);

        const $map = $("#map");

        $map.data("DG:map", map);
        $map.data("DG:markers", markers);
    }
});
