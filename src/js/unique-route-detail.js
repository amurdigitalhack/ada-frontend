const places = require("./places.js");
const getCookie = require("./cookies").getCookie;

console.dir(places);

$(() => {
    if($(".unique-route-detail-page").length){
        const DG = window.DG;
        const map = DG.map('2gis-map', {
            'center': [50.274641, 127.531443], // Благовещенск
            'zoom': 13
        });
        let query = JSON.parse(getCookie('routeQuery'));
        let route = generateRoute(query.interest, query.money, query.count_place);

        $(".place-list").html(generatePlaceListHTML(route));
        $(".route-info").html(generateRouteInfoHTML(route));

        let markers = DG.featureGroup();

        route.forEach(place => {
            DG.marker([place.lat, place.lng]).addTo(markers);
        });

        markers.addTo(map);
        map.fitBounds(markers.getBounds());
    }
});

function generateRoute(tags, money, placeCount) {
    let preset = places.filter(place => tags.includes(place.tag));

    preset.forEach((v,i,p)=> {
        let t, j = Math.floor(Math.random() * p.length);

        [p[i], p[j]] = [p[j], p[i]];
    });

    // sort((a, b) => a.money - b.money);

    let result = [];
    let sum = 0;

    preset.forEach(place => {

        if(sum + place.money > money) return;

        sum += place.money;

        result.push(place);
    });

    return result.slice(0, placeCount);
}

function generateRouteInfoHTML(route){
    return `Мест: <b>${route.length}</b><br/>Сумма: <b>${ route.reduce((s, p)=> s + p.money, 0) }</b> рублей`;
}

function generatePlaceListHTML(route){
    return `<div class="place-item">${ route.map((place, i) => `
                <div class="place-item">
                    <div class="row m-0 p-0">
                        <div class="col-1 item-area-num"><span>${ i+1 }</span></div>
                        <div class="col-11">
                            <div class="row">
                                <div class="col-8">
                                    <h5 class="text-uppercase">${ place.name }</h5>
                                </div>
                                <div class="col-3">
                                    <div class="stars" style="width: 100px"></div>
                                </div>
                                <div class="col-1 pr-0 pl-0 text-right">
                                    <a class="reverse-btn"></a>
                                    <a class="delete-btn"></a>
                                </div>
                                <div class="w-100"></div>
                                <div class="col-12">
                                    <p class="mb-2">${ place.description }</p>
                                    <p class="mb-2">${ place.address }</p>
                                    <a class="text-uppercase font-pfd-medium text-color-b0ca1f" href="#">Читать отзывы</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`).join("") }</div>`;
}