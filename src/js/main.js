window.$ = window.jQuery = require('jquery');
// window.Popper = require('popper.js').default;
require('bootstrap/dist/js/bootstrap.bundle.min');
window.Slider = require('bootstrap-slider/src/js/bootstrap-slider');
require('bootstrap-multiselect/dist/js/bootstrap-multiselect');

window._ = require('lodash');
window.DG = require("2gis-maps");

/* CUSTOM FILES GOES BELOW */

require('./include-map');
require('./click-events');
require('./best-route');
require('./unique-route');
require('./unique-route-detail');
