const setCookie = require("./cookies").setCookie;

$(() => {
    if($(".unique-route-page").length){
        new Slider('#slide-price', {
            formatter: function(value) {
                return 'Current value: ' + value;
            }
        });

        new Slider('#slide-count-place', {
            formatter: function(value) {
                return 'Current value: ' + value;
            }
        });

        $('.multiselect-native-select select').multiselect();

        $('#create-route').on('click',function(){

            let money = +$('input[data-slider-id=sp1]').val();

            let count_place = +$('input[data-slider-id=scp]').val();

            let interest = $('.multiselect-native-select .active input').map((i, e) => $(e).val()).get();
            // let interest1 = getMultiElements('example-getting-started-1');
            // let interest2 = getMultiElements('example-getting-started-2');
            let json = {
                money,
                count_place,
                interest
                // 'interest' : interest1.concat(interest2)
            };
            setCookie('routeQuery',JSON.stringify(json));

            window.location.href = '/unicumdetail.html'
        })
    }
});

// function getMultiElements(id){
//     return $(`#${id} ~ .btn-group ul li.active input`).map((i, e) => $(e).val()).get();
// }
