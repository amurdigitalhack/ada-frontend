$(() => {
    if($(".best-route-page").length){
        new Slider('#slide-price', {
            formatter: function(value) {
                return 'Current value: ' + value;
            }
        });

        $('#example-getting-started-1').multiselect();

        new Slider('#slide-count-place', {
            formatter: function(value) {
                return 'Current value: ' + value;
            }
        });
    }
});
